import {Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter} from '@angular/core';
import {Ingredient} from "../shared/ingredient";
import {ShoppingListService} from "./shopping-list.service";

@Component({
  selector: 'rb-shopping-list-add',
  templateUrl: './shopping-list-add.component.html'
})
export class ShoppingListAddComponent implements OnChanges {

  @Input() item: Ingredient;
  @Output() cleared = new EventEmitter();
  isAdd = true;
  constructor(private sls: ShoppingListService) { }

  ngOnChanges(changes): void {
   if (changes.item.currentValue === null) {
     this.isAdd = true;
     this.item = new Ingredient(null, null);
   } else {
     this.isAdd = false;
   }
  }

  onSubmit(ingredient: Ingredient) {
    const newItem = new Ingredient(ingredient.name, ingredient.amount);
    if (!this.isAdd) {
      this.sls.editItem(this.item, newItem);
      this.onClear();
    } else {
      this.item = newItem;
      this.sls.addItems([newItem]);
    }
  }

  onClear() {
    this.isAdd = true;
    this.cleared.emit(null);
  }

  onDelete() {
    this.sls.deleteItem(this.item);
    this.onClear();
  }

}
