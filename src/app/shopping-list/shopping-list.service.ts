import {Ingredient} from "../shared/ingredient";
export class ShoppingListService {
  private items: Ingredient[] = [];


  getItems() {
    return this.items;
  }

  addItems(items: Ingredient[]) {
    items.forEach(item => this.items.push(item));
  }

  editItem(oldItem: Ingredient, newItem: Ingredient) {
    this.items[this.items.indexOf(oldItem)] = newItem;
  }

  deleteItem(item: Ingredient) {
    this.items.splice(this.items.indexOf(item), 1);
  }
}
