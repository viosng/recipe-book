import {Injectable, EventEmitter} from '@angular/core';
import {Recipe} from "../recipe";
import {Ingredient} from "../shared/ingredient";
import {Headers, Http, Response} from "@angular/http";
import 'rxjs/Rx';

@Injectable()
export class RecipeService {
    recipesChanged = new EventEmitter<Recipe[]>();
    private recipes: Array<Recipe> = [
        new Recipe('Salad', 'Very healthy',
            'http://images.fitnessmagazine.mdpcdn.com/sites/fitnessmagazine.com/files/styles/slide/public/slide/R137559.jpg', [
                new Ingredient('tomato', 2),
                new Ingredient('cheese1', 1)
            ]),
        new Recipe('Dummy', 'Dummy recipe',
            'http://i.dailymail.co.uk/i/pix/2015/08/12/01/1AF9646D000005DC-3194442-image-a-21_1439339842820.jpg', [
                new Ingredient('dummy', 10)
            ])];

    constructor(private http: Http) {
    }

    getRecipes() {
        return this.recipes;
    }

    getRecipe(recipeIndex: number) {
        return this.recipes[recipeIndex];
    }

    deleteRecipe(recipe: Recipe) {
        this.recipes.splice(this.recipes.indexOf(recipe), 1);
    }

    addRecipe(recipe: Recipe) {
        this.recipes.push(recipe);
    }

    editRecipe(oldItem: Recipe, newItem: Recipe) {
        this.recipes[this.recipes.indexOf(oldItem)] = newItem;
    }

    storeData() {
        const body = JSON.stringify(this.recipes);
        const headers = new Headers({
            'Content-Type': 'application/json'
        });
        return this.http.put("https://recipe-book-bb424.firebaseio.com/recipes.json", body, {
            headers: headers
        })
    }

    retrieveData() {
        return this.http.get("https://recipe-book-bb424.firebaseio.com/recipes.json")
            .map((response: Response) => response.json())
            .subscribe(
                (data: Recipe[]) => {
                    this.recipes = data;
                    this.recipesChanged.emit(data);
                }
            )
    }
}
