import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {RecipeService} from "../recipe.service";
import {Subscription} from "rxjs";
import {Recipe} from "../../recipe";
import {FormArray, FormControl, Validators, FormBuilder, FormGroup} from "@angular/forms";
import {Ingredient} from "../../shared/ingredient";

@Component({
    selector: 'rb-recipe-edit',
    templateUrl: './recipe-edit.component.html'
})
export class RecipeEditComponent implements OnInit, OnDestroy {
    private subscription: Subscription;
    private recipeIndex: number;
    private recipe: Recipe;
    private isNew = false;
    recipeForm: FormGroup;

    constructor(private route: ActivatedRoute,
                private recipeService: RecipeService,
                private formBuilder: FormBuilder,
                private router: Router) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(
            (params: any) => {
                if (params.hasOwnProperty('id')) {
                    this.recipeIndex = +params['id'];
                    this.recipe = this.recipeService.getRecipe(this.recipeIndex);
                    this.isNew = false;
                } else {
                    this.isNew = true;
                    this.recipe = null;
                }
                this.initForm();
            }
        );
    }

    onSubmit() {
        const recipe: Recipe = this.recipeForm.value;
        if (this.isNew) {
            this.recipeService.addRecipe(recipe);
        } else {
            this.recipeService.editRecipe(this.recipe, recipe);
        }
        this.navigateBack();
    }

    navigateBack() {
        this.router.navigate(['../']);
    }

    onRemoveIngredient(i: number) {
        (<FormArray>this.recipeForm.controls['ingredients']).removeAt(i);
    }

    onAddIngredient(name: string, amount: string) {
        (<FormArray>this.recipeForm.controls['ingredients']).push(this.formBuilder.group({
            'name': [name, Validators.required],
            'amount': [amount, [Validators.required, Validators.pattern("\\d+")]]
        }));
        console.log(this.recipeForm.value);
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    private initForm() {
        let name = '';
        let imageUrl = '';
        let description = '';
        let ingredients: FormArray = new FormArray([]);

        if (!this.isNew) {
            ingredients = new FormArray(
                (this.recipe.hasOwnProperty('ingredients') ? this.recipe.ingredients : [])
                    .map(ingredient => this.formBuilder.group({
                        'name': [ingredient.name, Validators.required],
                        'amount': [ingredient.amount, [Validators.required, Validators.pattern("\\d+")]]
                    })));
            name = this.recipe.name;
            imageUrl = this.recipe.imagePath;
            description = this.recipe.description;
        }
        this.recipeForm = this.formBuilder.group({
            'name': [name, Validators.required],
            'imagePath': [imageUrl, Validators.required],
            'description': [description, Validators.required],
            'ingredients': ingredients,
        });
    }

}
